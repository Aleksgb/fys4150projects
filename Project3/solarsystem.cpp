#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <cmath>
#include <armadillo>
#include <time.h>
#include "planet.h"
#include "system.h"

using namespace arma;
using namespace std;

void solarsystem()
{
    int n = 10;

    Planet plist[n];
    // Here we give the planet-instances to the instance-list plist:
    // Attributes are: Name, mass, aphelion-distance, minimal velocity and start-angle
    plist[0] = Planet("Sun", 1, 0, 0.00245, 110.2);
    plist[1] = Planet("Mercury", 1.66e-7, 0.4655, 8.1699, 350);
    plist[2] = Planet("Venus", 2.448e-6, 0.7263, 7.314, 45);
    plist[3] = Planet("Earth", 3.0e-6, 1.014, 6.158, 20);
    plist[4] = Planet("Mars", 3.2268e-7, 1.6615, 4.619, 250);
    plist[5] = Planet("Jupiter", 9.545e-4, 5.444, 2.615, 285);
    plist[6] = Planet("Saturn", 2.8578e-4, 10.097, 1.911, 0);
    plist[7] = Planet("Uranus", 4.3643e-5, 20.024, 1.364, 75);
    plist[8] = Planet("Neptune", 5.1496e-5, 30.304, 1.129, 295);
    plist[9] = Planet("Pluto", 6.5608e-9, 49.173, 0.78, 295);

    System threebody(n, plist);
    threebody.rk4(0.01, 50.0, "solarsystem.dat");

    system("python solarsystem.py");
}
