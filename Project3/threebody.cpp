#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <cmath>
#include <armadillo>
#include <time.h>
#include "planet.h"
#include "system.h"

using namespace arma;
using namespace std;

void threebody()
{
    int n = 3;

    Planet plist[n];
    // Here we give the planet-instances to the instance-list plist:
    // Attributes are: Name, mass, aphelion-distance, minimal velocity and start-angle
    plist[0] = Planet("Sun", 1, 0, 0.02499, 105.2);
    plist[1] = Planet("Earth", 3.0e-6, 1.014, 6.158, 20);
    plist[2] = Planet("Jupiter", 9.545e-3, 5.444, 2.615, 285);

    System threebody(n, plist);
    threebody.rk4(0.071, 30.0, "threebody.dat");
    system("python threebody.py");
}
