#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <cmath>
#include <armadillo>
#include <time.h>
#include "planet.h"
#include "system.h"

using namespace arma;
using namespace std;

void twobody()
{
    int n = 2;

    Planet plist[n];
    // Here we give the planet-instances to the instance-list plist:
    // Attributes are: Name, mass, aphelion-distance, minimal velocity and start-angle
    plist[0] = Planet("Sun", 1, 0, 0, 0);
    plist[1] = Planet("Earth", 3.0e-6, 1.014, 6.158, 20);

    System twobody(n, plist);
    twobody.rk4(0.075, 30.0, "twobody.dat");
    system("python twobody.py");
}
