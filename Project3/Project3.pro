TEMPLATE = app
CONFIG += console
CONFIG -= qt

SOURCES += main.cpp \
    system.cpp \
    planet.cpp \
    testing.cpp \
    twobody.cpp \
    threebody.cpp \
    solarsystem.cpp

HEADERS += \
    system.h \
    planet.h \
    testing.h \
    twobody.h \
    threebody.h \
    solarsystem.h

