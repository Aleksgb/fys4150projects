#include "system.h"
#include "planet.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <cmath>
#include <armadillo>
#include <time.h>

using namespace std;


System::System(int n, Planet *plist)
{/* Constructor which stores the number of planets,
    a list of planet-instances and puts the masses and
    initial values in two 2D arrays*/

    NumberOfPlanets = n;
    PlanetList = plist;
    Masses = vec(n);
    yvalues = mat(4, n);
    for (int i = 0; i < n; i++) {
        Masses(i) = PlanetList[i].PlanetMass;
        yvalues(0, i) = PlanetList[i].PlanetPosition[0];
        yvalues(1, i) = PlanetList[i].PlanetPosition[1];
        yvalues(2, i) = PlanetList[i].PlanetVelocity[0];
        yvalues(3, i) = PlanetList[i].PlanetVelocity[1];
    }
}

void System::CalcTotEnAngMom(mat &in, vec &out)
{
    /* This function calculate the total energy of the
       system as well as the total angular momentum of
       the system.
       Input:           Output:
       in(0,i) = xi     out(0) = Etot
       in(1,i) = yi     out(1) = Ltot
       in(2,i) = vxi
       in(3,i) = vyi                                */
    out.fill(0);
    for (int i = 0; i < NumberOfPlanets; i++) {
        out(0) += 0.5*Masses(i)*(in(2,i)*in(2,i) + in(3,i)*in(3,i));
        out(1) += Masses(i)*(in(0,i)*in(3,i) - in(1,i)*in(2,i));
        for (int j = 0; j < NumberOfPlanets; j++) {
            if (j > i) {
                out(0) -= G*Masses(i)*Masses(j)/(sqrt(pow(in(0,j) - in(0,i), 2) + pow(in(1,j) - in(1,i), 2)));
            }
        }
    }
}

mat System::derivatives(mat &fin)
{   /* This function finds the right hand sides
    of the coubled differential equations
    Input:           Output:
    fin(0,i) = xi    fout(0,i) = vxi
    fin(1,i) = yi    fout(1,i) = vyi
    fin(2,i) = vxi   fout(2,i) = fx(xi, xj, yi, yj)
    fin(3,i) = vyi   fout(3,i) = fy(xi, xj, yi, yj) */

    mat fout(4, NumberOfPlanets);
    fout.fill(0);
    for (int i = 0; i < NumberOfPlanets; i++) {
        fout(0,i) = fin(2,i); //
        fout(1,i) = fin(3,i);
        for (int j = 0; j < NumberOfPlanets; j++) {
            if (i != j){
                fout(2,i) += G*(Masses(j)*(fin(0,j) - fin(0,i))/(pow(sqrt(pow(fin(0,j) - fin(0,i), 2) + pow(fin(1,j) - fin(1,i), 2)), 3)));
                fout(3,i) += G*(Masses(j)*(fin(1,j) - fin(1,i))/(pow(sqrt(pow(fin(0,j) - fin(0,i), 2) + pow(fin(1,j) - fin(1,i), 2)), 3)));
            }
        }
    }
    return fout;
}


void System::rk4_step(double dt, mat &yin)
{   /* This function calculates one step of the fourth order
    Runge Kutta-method. More precisely it calculates the value y(t + delta_t)
    Input: time t and the stepsize delta_t, yin (values of xi, yi, vxi, vyi at time t)
    Output: yout (values of xi, yi, vxi, vyi at time t + delta_t) */

    mat k1(4, NumberOfPlanets);
    mat k2(4, NumberOfPlanets);
    mat k3(4, NumberOfPlanets);
    mat k4(4, NumberOfPlanets);
    mat y_k(4, NumberOfPlanets);

    k1 = derivatives(yin)*dt;
    k4 = yin + 0.5*k1;
    k2 = derivatives(k4)*dt;
    k4 = yin + 0.5*k2;
    k3 = derivatives(k4)*dt;
    k4 = yin + k3;
    k4 = derivatives(k4)*dt;
    yin += 1.0/6.0*(k1 + 2*(k2 + k3) + k4);
}


void System::rk4(double dt, double years, char *filename)
{   /* We are using the fourth order Runge Kutta algorithm.
    The input to this method is the timestep, the number of
    years to look at and the filename to write the data to.
    we loop over the number of steps and send dt and yin
    to the method rk4_step. we have that
                yin(0,i) = xi
                yin(1,i) = yi
                yin(2,i) = vxi
                yin(3,i) = vyi
    and the returned values are the new values of these.*/

    ofstream outFile;
    outFile.open(filename, ios::out);
    outFile << "The first column is t_h, then four and four columns belong together. First comes xi, then yi, vxi and vyi for planet i. The two last columns are the total energy and the total angular momentum for the whole system respectively. The planets are respectively: ";
    for (int i = 0; i < NumberOfPlanets; i++) {
        if (i == NumberOfPlanets-1) {
            outFile << PlanetList[i].PlanetName << endl;
        }
        else {
            outFile << PlanetList[i].PlanetName << ", ";
        }
    }
    double t_h;
    double nstep = years/dt;
    mat yin(4, NumberOfPlanets);
    for (int i = 0; i < NumberOfPlanets; i++) {
        for (int j = 0; j < 4; j++) {
            yin(j, i) = yvalues(j, i);
        }
    }
    t_h = 0;
    outFile.setf(ios::scientific);
    outFile.precision(20);
    vec EL(2);
    for (int i = 1; i <= nstep; i++) {
        CalcTotEnAngMom(yin, EL);
        rk4_step(dt, yin);
        outFile << i*dt << " ";
        t_h += dt;
        for (int i = 0; i < NumberOfPlanets; i++) {
            for (int j = 0; j < 4; j++) {
                outFile << yin(j, i) << " ";
            }
        }
        outFile << EL(0) << " " << EL(1);
        outFile << endl;
    }
    outFile.close();
}
