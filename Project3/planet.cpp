#include "planet.h"

Planet::Planet(){}

Planet::Planet(char *name, double mass, double aphelion, double vmin, double angle)
{
    double pi = 4*atan(1);
    angle *= 2*pi/360.0;

    PlanetName = name;
    PlanetMass = mass;
    PlanetPosition[0] = cos(angle)*aphelion;
    PlanetPosition[1] = sin(angle)*aphelion;
    PlanetVelocity[0] = -sin(angle)*vmin;
    PlanetVelocity[1] = cos(angle)*vmin;
}
