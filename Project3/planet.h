#ifndef PLANET_H
#define PLANET_H

#include <armadillo>

using namespace arma;

class Planet
{
public:
    char *PlanetName;
    double PlanetMass;
    double PlanetPosition[2], PlanetVelocity[2];
    Planet();
    Planet(char *, double, double, double, double);
};

#endif // PLANET_H
