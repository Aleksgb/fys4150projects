#ifndef SYSTEM_H
#define SYSTEM_H

#include <armadillo>
#include <math.h>
#include "planet.h"
#include <math.h>

using namespace arma;

class System
{
private:
    int NumberOfPlanets;  // Number of planets
    Planet* PlanetList;  // List of planet-instances
    double G = 4*16*atan(1)*atan(1); // Gravitational constant with units AU^3/(kg*yr^2)


public:
    vec Masses;
    mat yvalues;
    System(int, Planet *);
    void CalcTotEnAngMom(mat &, vec &);
    mat derivatives(mat &);
    void rk4_step(double, mat &);
    void rk4(double, double, char *);
};

#endif // SYSTEM_H
