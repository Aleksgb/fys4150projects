from scitools.std import *
from numpy import loadtxt

n, iterations, arma, jacobi = loadtxt('Project2niterations.dat', skiprows=1, unpack=True)
plot(n, iterations, '-o', xlabel='Number of steps', ylabel='Number of iterations', hardcopy='niterations.png')
figure()
plot(n, arma, '-ro', n, jacobi, '-bo', xlabel='Number of steps', ylabel='Time used [s]', legend=('Armadillo', 'Jacobi'), hardcopy='timeusage.png')
