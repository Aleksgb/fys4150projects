from scitools.std import *
from numpy import loadtxt

rho1, eigvec1 = loadtxt('Eigenvecwr=0.01.dat', skiprows=1, unpack=True)
rho2, eigvec2 = loadtxt('Eigenvecwr=0.05.dat', skiprows=1, unpack=True)
rho3, eigvec3 = loadtxt('Eigenvecwr=0.25.dat', skiprows=1, unpack=True)
rho4, eigvec4 = loadtxt('Eigenvecwr=0.5.dat', skiprows=1, unpack=True)
rho5, eigvec5 = loadtxt('Eigenvecwr=1.0.dat', skiprows=1, unpack=True)
rho6, eigvec6 = loadtxt('Eigenvecwr=5.0.dat', skiprows=1, unpack=True)

<<<<<<< HEAD
plot(rho, eigvec, '-ro', rho2, eigvec2, '-bo', xlabel='Dimensionless distance', ylabel='Probability distribution', legend=('Non-interacting electrons', 'Interacting electrons'), hardcopy='eigvecswr=0.25.png')
=======
>>>>>>> e4de210db44d98018e7cf483c5c295aca07a6cb3

plot(rho6, eigvec6**2, rho5, eigvec5**2, rho4, eigvec4**2, rho3, eigvec3**2, rho2, eigvec2**2, rho1, eigvec1**2, xlabel='Dimensionless distance', ylabel='Probability distribution', legend=('Interaction with wr = 5.0', 'Interaction with wr = 1.0', 'Interaction with wr = 0.5', 'Interaction with wr = 0.25', 'Interaction with wr = 0.05', 'Interaction with wr = 0.01'), hardcopy='eigvecsplot.png')
