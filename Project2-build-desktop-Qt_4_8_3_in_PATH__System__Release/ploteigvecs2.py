from scitools.std import *
from numpy import loadtxt

rho1, eigvec1 = loadtxt('Eigenvecnonintwr=0.01.dat', skiprows=1, unpack=True)
rho2, eigvec2 = loadtxt('Eigenvecnonintwr=0.05.dat', skiprows=1, unpack=True)
rho3, eigvec3 = loadtxt('Eigenvecnonintwr=0.25.dat', skiprows=1, unpack=True)
rho4, eigvec4 = loadtxt('Eigenvecnonintwr=0.5.dat', skiprows=1, unpack=True)
rho5, eigvec5 = loadtxt('Eigenvecnonintwr=1.0.dat', skiprows=1, unpack=True)
rho6, eigvec6 = loadtxt('Eigenvecnonintwr=5.0.dat', skiprows=1, unpack=True)


plot(rho6, eigvec6**2, rho5, eigvec5**2, rho4, eigvec4**2, rho3, eigvec3**2, rho2, eigvec2**2, rho1, eigvec1**2, xlabel='Dimensionless distance', ylabel='Probability distribution', legend=('wr = 5.0', 'wr = 1.0', 'wr = 0.5', 'wr = 0.25', 'wr = 0.05', 'wr = 0.01'), hardcopy='eigvecsplot2.png')
