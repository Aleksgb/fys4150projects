#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <cmath>
#include <armadillo>
#include <time.h>
#include "lib.h"
#include "legendre.h"
#include "hermite.h"
#include "brutemc.h"
#include "impsamp.h"
#include "legpar.h"
#include "impsamppar.h"
#include "hermpar.h"
#include "brutemcpar.h"
#include "metropolis.h"

using namespace std;
using namespace arma;

int main(int nargs, char* args[])
{
    metropolis(nargs, args);
}

