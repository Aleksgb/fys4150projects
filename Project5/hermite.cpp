#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <cmath>
#include <armadillo>
#include <time.h>
#include "lib.h"

using namespace std;
using namespace arma;

void GaussHermite(double *,double *, int);
double funcherm(double x1, double y1, double z1, double x2, double y2, double z2);

void hermite()
{
    clock_t start, finish;
    for (int n = 5; n <= 50; n += 5) {
        start = clock();
        double *x = new double [n+1];
        double *w = new double [n+1];
        GaussHermite(x, w, n);
        double integral = 0;

        for (int i=1; i<n+1; i++) {
            for (int j=1; j<n+1; j++) {
                for (int k=1; k<n+1; k++) {
                    for (int i2=1; i2<n+1; i2++) {
                        for (int j2=1; j2<n+1; j2++) {
                            for (int k2=1; k2<n+1; k2++) {
                                integral += w[i]*w[j]*w[k]*w[i2]*w[j2]*w[k2]*funcherm(x[i], x[j], x[k], x[i2], x[j2], x[k2]);
                            }
                        }
                    }
                }
            }
        }

        finish = clock();
        double cputime = (finish - start)/double(CLOCKS_PER_SEC);
        cout << "for n = " << n << " The integral is " << integral << " Time used is " << cputime << " seconds " << endl;
        delete [] x;
        delete [] w;
    }
}


void GaussHermite(double *x,double *w, int n)
{
    int i,its,j,m;
    double p1,p2,p3,pp,z,z1;
    #define EPS 3.0e-14
    #define PIM4 0.7511255444649425
    #define MAXIT 10

    m=(n+1)/2;
    for (i=1;i<=m;i++) {
        if (i == 1) {
            z=sqrt((double)(2*n+1))-1.85575*pow((double)(2*n+1),-0.16667);
        } else if (i == 2) {
            z -= 1.14*pow((double)n,0.426)/z;
        } else if (i == 3) {
            z=1.86*z-0.86*x[1];
        } else if (i == 4) {
            z=1.91*z-0.91*x[2];
        } else {
            z=2.0*z-x[i-2];
        }
        for (its=1;its<=MAXIT;its++) {
            p1=PIM4;
            p2=0.0;
            for (j=1;j<=n;j++) {
                p3=p2;
                p2=p1;
                p1=z*sqrt(2.0/j)*p2-sqrt(((double)(j-1))/j)*p3;
            }
            pp=sqrt((double)2*n)*p2;
            z1=z;
            z=z1-p1/pp;
            if (fabs(z-z1) <= EPS) break;
        }
        if (its > MAXIT) cout << "MAXIT reached!" << endl;
        x[i]=z;
        x[n+1-i] = -z;
        w[i]=2.0/(pp*pp);
        w[n+1-i]=w[i];
    }
}


double funcherm(double x1, double y1, double z1, double x2, double y2, double z2)
{
    double value;

    if (x1 == x2 && y1 == y2 && z1 == z2) {
        value = 0;
    }
    else {
        double x = x1-x2;
        double y = y1-y2;
        double z = z1-z2;
        value = 1./sqrt(x*x + y*y + z*z);
    }

    return value;
}
