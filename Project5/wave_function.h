#ifndef WAVE_FUNCTION_T2_H
#define WAVE_FUNCTION_T2_H

#include <armadillo>
using namespace arma;

double wave_function(mat &, double, double);

#endif // WAVE_FUNCTION_T2_H
