#ifndef LOCAL_ENERGY_H
#define LOCAL_ENERGY_H

#include <armadillo>
using namespace std;

double local_energy(mat &, double, double, double);

#endif // LOCAL_ENERGY_H
