#include <mpi.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <cmath>
#include <armadillo>
#include <time.h>
#include "lib.h"

using namespace std;
using namespace arma;

double funcimpsamppar(double *x);

// function for gaussian random numbers
double gaussian_deviatepar(long *);

void impsamppar(int nargs, char* args[])
{
    long int i, j, n;
    long idum;
    double local_brute_mc, x[6], f, local_sum_sigma, variance, jacobidet;
    int numprocs, my_rank;
    double total_sum_sigma, total_brute_mc, start, finish;

    MPI_Init(&nargs, &args);
    MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

    jacobidet = pow(acos(-1), 3);

    for (n = 1e0; n <= 1e9; n *= 10) {
        start = MPI_Wtime();
        local_brute_mc = 0;
        local_sum_sigma = 0;
        idum = -1-my_rank;

        for (i = my_rank; i < n; i += numprocs) {
            for (j = 0; j<=5; j++) {
                x[j] = gaussian_deviatepar(&idum)/sqrt(2.);
            }
            f = funcimpsamppar(x);
            local_brute_mc += f;
            local_sum_sigma += f*f;
        }
        MPI_Reduce(&local_brute_mc, &total_brute_mc, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
        MPI_Reduce(&local_sum_sigma, &total_sum_sigma, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
        if (my_rank == 0) {
            total_brute_mc /= (double) n;
            total_sum_sigma /= (double) n;
            variance = total_sum_sigma - total_brute_mc*total_brute_mc;
            finish = MPI_Wtime();
            double cputime = finish - start;
            cout << "For n = " << n << ", standard deviation = " << jacobidet*sqrt(variance/(double) n) << ", integral = " << jacobidet*total_brute_mc << ", Time used is " << cputime << " seconds " << endl;
        }
    }
    MPI_Finalize();
}


double funcimpsamppar(double *x)
{
    double value;

    if (x[0] == x[1] && x[2] == x[3] && x[4] == x[5]) {
        value = 0;
    }
    else {
        double xx = x[0]-x[1];
        double y = x[2]-x[3];
        double z = x[4]-x[5];
        value = 1./sqrt(xx*xx + y*y + z*z);
    }

    return value;
}



// random numbers with gaussian distribution
double gaussian_deviatepar(long * idum)
{
  static int iset = 0;
  static double gset;
  double fac, rsq, v1, v2;

  if ( idum < 0) iset =0;
  if (iset == 0) {
    do {
      v1 = 2.*ran2(idum) -1.0;
      v2 = 2.*ran2(idum) -1.0;
      rsq = v1*v1+v2*v2;
    } while (rsq >= 1.0 || rsq == 0.);
    fac = sqrt(-2.*log(rsq)/rsq);
    gset = v1*fac;
    iset = 1;
    return v2*fac;
  } else {
    iset =0;
    return gset;
  }
} // end function for gaussian deviates




