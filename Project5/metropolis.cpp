#include <mpi.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <cmath>
#include <armadillo>
#include <time.h>
#include "lib.h"
#include "wave_function.h"
#include "local_energy.h"

using namespace arma;


// Mc sampling with the Metropolis test
void metropolis(int nargs, char* args[])
{
    ofstream outFile;
    outFile.open("Project5.dat", ios::out);
    int i, j, var, samples, accept, numprocs, my_rank;
    int max_samples = 100000;
    int thermalization = 100000;
    long idum;
    double alphastep = 0.01;
    double maxvar = 1./alphastep + 2;
    double alpha = 0.5-alphastep;
    double beta = 0;
    double step = 0.5;
    double wfold, wfnew, loc_energy, total_energy, loc_energy2, total_energy2, delta_e, start, finish;
    mat r_old = zeros(2,3);
    mat r_new = zeros(2,3);
    vec mean = zeros(maxvar-1);
    vec stdev = zeros(maxvar-1);

    MPI_Init(&nargs, &args);
    MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

    idum = -1 - my_rank;

    // Loop over variations of alpha
    for (int var = 1; var < maxvar; var++) {
        start = MPI_Wtime();
        alpha += alphastep;
        loc_energy = loc_energy2 = total_energy = total_energy2 = accept = delta_e = 0;

        // Initial position between -0.5 and 0.5
        for (i = 0; i < 2; i++) {
            for (j = 0; j < 3; j++) {
                r_old(i,j) = step*(ran1(&idum) - 0.5);
            }
        }
        wfold = wave_function(r_old, alpha, beta);

        // Loop over Monte Carlo samples
        for (samples = my_rank; samples <= max_samples + thermalization; samples += numprocs) {

            // Calculate new position
            for (i = 0; i < 2; i++) {
                for (j = 0; j < 3; j++) {
                    r_new(i, j) = r_old(i, j) + step*(ran1(&idum) - 0.5);
                }
            }
            wfnew = wave_function(r_new, alpha, beta);

            // Perform the Metropolis test
            if (ran1(&idum) <= wfnew*wfnew/(wfold*wfold)) {
                r_old = r_new;
                wfold = wfnew;
                accept++;
            }

            // Calculate and update the local energy
            if (samples > thermalization) {
                delta_e = local_energy(r_old, alpha, beta, wfold);
                loc_energy += delta_e;
                loc_energy2 += delta_e*delta_e;
            }
        } // End of MC sampling

        MPI_Reduce(&loc_energy, &total_energy, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
        MPI_Reduce(&loc_energy2, &total_energy2, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
        if (my_rank == 0) {
            cout << "Variational parameter = " << alpha << "\npercent accepted = " << accept/((double) max_samples+thermalization) <<"\n";
            total_energy /= max_samples;
            total_energy2 /= max_samples;
            mean(var-1) = total_energy;
            stdev(var-1) = (total_energy2 - total_energy*total_energy)/(double) max_samples;
            finish = MPI_Wtime();
            double cputime = finish - start;
            cout << "Mean energy = " << mean(var-1) << "\nStd. dev. = " << stdev(var-1) << "\ntime = " << cputime << "\n\n";
            outFile << mean(var-1) << " " << stdev(var-1) << " " << cputime << endl;
        }
    } // End of alpha-variation
    MPI_Finalize();
}
