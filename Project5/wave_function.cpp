#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <cmath>
#include <armadillo>
#include <time.h>
#include "lib.h"

using namespace arma;

double wave_function(mat &r, double alpha, double beta) {
    double argument = 0.0;
    double r12 = 0.0;
    double wf = 0.0;
    for (int j = 0; j < 3; j++) {
        for (int i = 0; i < 2; i++) {
            argument += r(i,j)*r(i,j);
        }
        r12 += (r(0,j)-r(1,j))*(r(0,j)-r(1,j));
    }
    r12 = sqrt(r12);
    if (beta != 0) {
        wf = exp(-alpha*alpha*argument/2.)*exp(r12/(2.*(1 + beta*r12)));
    }
    else {
        wf = exp(-alpha*alpha*argument/2.);
    }

    return wf;
}

