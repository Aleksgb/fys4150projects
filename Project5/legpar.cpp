#include <mpi.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <cmath>
#include <armadillo>
#include <time.h>
#include "lib.h"

using namespace std;
using namespace arma;

double funclegpar(double x1, double y1, double z1, double x2, double y2, double z2);

void legpar(int nargs, char *args [])
{
    double a = -4.0;
    double b = 4.0;
    int numprocs, my_rank;
    double total_sum, local_sum, start, finish;

    MPI_Init(&nargs, &args);
    MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

    for (int n = 5; n <= 50; n += 5) {
        start = MPI_Wtime();
        double *x = new double [n];
        double *w = new double [n];
        gauleg(a, b, x, w, n);
        total_sum = 0.0;
        local_sum = 0.0;

        for (int i=my_rank; i<n; i += numprocs) {
            for (int j=0; j<n; j++) {
                for (int k=0; k<n; k++) {
                    for (int i2=0; i2<n; i2++) {
                        for (int j2=0; j2<n; j2++) {
                            for (int k2=0; k2<n; k2++) {
                                local_sum += w[i]*w[j]*w[k]*w[i2]*w[j2]*w[k2]*funclegpar(x[i], x[j], x[k], x[i2], x[j2], x[k2]);
                            }
                        }
                    }
                }
            }
        }

        MPI_Reduce(&local_sum, &total_sum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
        finish = MPI_Wtime();
        double total_time = finish - start;
        if (my_rank == 0) {
            cout << "for n = " << n << " The integral is " << total_sum << " Time used is " << total_time << " seconds for " << numprocs << " processors" << endl;
        }
        delete [] x;
        delete [] w;
    }
    MPI_Finalize();
}

double funclegpar(double x1, double y1, double z1, double x2, double y2, double z2)
{
    double value;

    if (x1 == x2 && y1 == y2 && z1 == z2) {
        value = 0;
    }
    else {
        double x = x1-x2;
        double y = y1-y2;
        double z = z1-z2;
        value = exp(-(x1*x1 + y1*y1 + z1*z1 + x2*x2 + y2*y2 + z2*z2))/sqrt(x*x + y*y + z*z);
    }

    return value;
}
