#include <mpi.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <cmath>
#include <armadillo>
#include <time.h>
#include "lib.h"

using namespace std;
using namespace arma;

double funcbrutemcpar(double *x);


void brutemcpar(int nargs, char* args[])
{
    long int i, j, n;
    long idum;
    double a, b, local_brute_mc, x[6], f, local_sum_sigma, variance, jacobidet;
    int numprocs, my_rank;
    double total_sum_sigma, total_brute_mc, start, finish;

    MPI_Init(&nargs, &args);
    MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

    a = -3.5;
    b = 3.5;
    jacobidet = pow(b-a, 6);

    for (n = 1e0; n <= 1e9; n *= 10) {
        start = MPI_Wtime();
        local_brute_mc = 0;
        local_sum_sigma = 0;
        idum = -1-my_rank;

        for (i = my_rank; i < n; i += numprocs) {
            for (j = 0; j<=5; j++) {
                x[j] = a + (b-a)*ran0(&idum);
            }
            f = funcbrutemcpar(x);
            local_brute_mc += f;
            local_sum_sigma += f*f;
        }
        MPI_Reduce(&local_brute_mc, &total_brute_mc, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
        MPI_Reduce(&local_sum_sigma, &total_sum_sigma, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
        if (my_rank == 0) {
            total_brute_mc /= (double) n;
            total_sum_sigma /= (double) n;
            variance = total_sum_sigma - total_brute_mc*total_brute_mc;
            finish = MPI_Wtime();
            double cputime = finish - start;
            cout << "For n = " << n << ", standard deviation = " << jacobidet*sqrt(variance/(double) n) << ", integral = " << jacobidet*total_brute_mc << ", Time used is " << cputime << " seconds " << endl;
        }
    }
    MPI_Finalize();
}


double funcbrutemcpar(double *x)
{
    double value;

    if (x[0] == x[1] && x[2] == x[3] && x[4] == x[5]) {
        value = 0;
    }
    else {
        double xx = x[0]-x[1];
        double y = x[2]-x[3];
        double z = x[4]-x[5];
        value = exp(-(x[0]*x[0] + x[1]*x[1] + x[2]*x[2] + x[3]*x[3] + x[4]*x[4] + x[5]*x[5]))/sqrt(xx*xx + y*y + z*z);
    }

    return value;
}
