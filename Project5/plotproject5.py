from scitools.std import *

mean, stdev, time = loadtxt('Project5.dat', unpack=True)
alpha = linspace(0.5, 1.5, 11)
plot(alpha, mean, alpha, stdev, alpha, time, legend=('Mean', 'Standard deviation', 'Time'))


'''
n = array([5, 10, 15, 20, 25, 30, 35, 40, 45, 50])
n2 = array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
iherm = array([19.82, 22.14, 22.97, 23.40, 23.66, 23.84, 23.97, 24.06, 24.14, 24.19])
therm = array([0, 0.06, 0.68, 3.65, 13.16, 34.04, 83.73, 185.57, 380.49, 670.87])
ileg = array([10.34, 19.00, 22.06, 23.19, 23.73, 24.03, 24.22, 24.34, 24.42, 24.48])
tleg = array([0, 0.11, 1.03, 5.78, 22.36, 65.52, 176.17, 383.49, 803.22, 1846.83])
imc = array([1.19e-5, 9.41, 0.99, 20.49, 30.54, 25.62, 24.33, 24.29, 24.54, 24.65])
tmc = array([0, 0, 0, 0, 0, 0.02, 0.21, 2.24, 22.3, 241.49])
iimp = array([26.72, 25.70, 25.12, 24.19, 24.93, 24.67, 24.72, 24.74, 24.74, 24.74])
timp = array([0, 0, 0, 0, 0.01, 0.08, 0.8, 8.07, 86.15, 842.7])
vmc = array([0, 6.70, 0.73, 11.50, 10.05, 2.28, 0.81, 0.27, 0.09, 0.03])
vimp = array([0, 3.47, 2.25, 0.49, 0.18, 0.058, 0.018, 0.0059, 0.0019, 0.00059])
tlegpar = array([0, 0.06, 0.62, 3.28, 14.15, 38, 99.99, 216.72, 440.82, 921.928])
timppar = array([0, 0, 0, 0, 0.017, 0.076, 0.43, 4.41, 44.38, 444.56])
thermpar = array([0, 0.056, 0.31, 1.50, 6.11, 18.44, 46.98, 99.29, 197.11, 356.64])
tmcpar = array([0, 0, 0, 0, 0, 0.015, 0.13, 1.29, 12.50, 126.24])


plot(n, iherm, n, ileg, xlabel=('Mesh points'), ylabel=('Integral value'), legend=('Gauss-Hermite', 'Gauss-Legendre'), hardcopy=('intvsmesh.png'))
figure()
plot(n, therm, n, tleg, xlabel=('Mesh points'), ylabel=('Time used [s]'), legend=('Gauss-Hermite', 'Gauss-Legendre'), hardcopy=('timevsmesh.png'))
figure()
plot(n2, imc, n2, iimp, xlabel=('Logarithm of the number of samples'), ylabel=('Integral value'), legend=('Brute force Monte Carlo', 'Importance sampling'), hardcopy=('intvsmeshmc.png'))
figure()
plot(n2, tmc, n2, timp, xlabel=('Logarithm of the number of samples'), ylabel=('Time used [s]'), legend=('Brute force Monte Carlo', 'Importance sampling'), hardcopy=('timevsmeshmc.png'))
figure()
plot(n2, vmc, n2, vimp, xlabel=('Logarithm of the number of samples'), ylabel=('Standard deviation'), legend=('Brute force Monte Carlo', 'Importance sampling'), hardcopy=('varvsmeshmc.png'))
plot(n, tleg, n, tlegpar, xlabel=('Mesh points'), ylabel=('Time usage [s]'), legend=('Gauss-Legendre', 'Parallellized Gauss-Legendre'), hardcopy=('timevsmeshpar.png'))
figure()
plot(n, therm, n, thermpar, xlabel=('Mesh points'), ylabel=('Time usage [s]'), legend=('Gauss-Hermite', 'Parallellized Gauss-Hermite'), hardcopy=('timevsmeshparherm.png'))
figure()
plot(n2, timp, n2, timppar, xlabel=('Logarithm of the number of sampling points'), ylabel=('Time usage [s]'), legend=('Importance sampling', 'Parallellized importance sampling'), hardcopy=('timevsmeshparimp.png'))
figure()
plot(n2, tmc, n2, tmcpar, xlabel=('Logarithm of the number of sampling points'), ylabel=('Time usage [s]'), legend=('Brute force Monte Carlo', 'Parallellized Brute Force Monte Carlo'), hardcopy=('timevsmeshparmc.png'))
'''
