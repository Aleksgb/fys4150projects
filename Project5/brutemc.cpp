#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <cmath>
#include <armadillo>
#include <time.h>
#include "lib.h"

using namespace std;
using namespace arma;

double funcmc(double *x);


void brutemc()
{
    clock_t start, finish;
    long int i, j, n;
    long idum;
    double brute_mc, x[6], a, b, f, sum_sigma, variance, jacobidet;

    a = -3.5;
    b = 3.5;
    jacobidet = pow(b-a, 6);

    for (n = 1e0; n <= 1e9; n *= 10) {
        start = clock();
        brute_mc = 0;
        sum_sigma = 0;
        idum = -2;

        for (i = 1; i <= n; i++) {
            for (j = 0; j<=5; j++) {
                x[j] = a + (b-a)*ran0(&idum);
            }
            f = funcmc(x);
            brute_mc += f;
            sum_sigma += f*f;
        }

        brute_mc /= (double) n;
        sum_sigma /= (double) n;
        variance = sum_sigma - brute_mc*brute_mc;
        finish = clock();
        double cputime = (finish - start)/double(CLOCKS_PER_SEC);
        cout << "For n = " << n << ", variance = " << jacobidet*sqrt(variance/(double) n) << ", integral = " << jacobidet*brute_mc << " Time used is " << cputime << " seconds " << endl;
    }
}


double funcmc(double *x)
{
    double value;

    if (x[0] == x[1] && x[2] == x[3] && x[4] == x[5]) {
        value = 0;
    }
    else {
        double xx = x[0]-x[1];
        double y = x[2]-x[3];
        double z = x[4]-x[5];
        value = exp(-(x[0]*x[0] + x[1]*x[1] + x[2]*x[2] + x[3]*x[3] + x[4]*x[4] + x[5]*x[5]))/sqrt(xx*xx + y*y + z*z);
    }

    return value;
}

