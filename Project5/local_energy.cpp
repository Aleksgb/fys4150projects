#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <cmath>
#include <armadillo>
#include <time.h>
#include "lib.h"
#include "wave_function.h"

using namespace arma;


double local_energy(mat &r, double alpha, double beta, double wfold) {
    int i, j;
    double e_loc, osc_term, wfminus, wfplus, e_kin, e_pot;
    double h = 0.0001;
    double r12 = 0;

    // r_h and r_-h to be used in the numerical second derivative
    mat r_minus(2,3), r_plus(2,3);
    r_minus = r_plus = r;

    // Compute the kinetic energy
    e_kin = 0.0;
    for (i = 0; i < 2; i++) {
        for (j = 0; j < 3; j++) {
            r_plus(i, j) = r(i,j) + h;
            r_minus(i, j) = r(i,j) - h;
            wfplus = wave_function(r_plus, alpha, beta);
            wfminus = wave_function(r_minus, alpha, beta);
            e_kin -= (wfminus+wfplus-2*wfold);
            r_plus(i, j) = r(i,j);
            r_minus(i, j) = r(i,j);
        }
    }
    e_kin = 0.5*e_kin/(h*h*wfold);

    // Compute the potential energy
    e_pot = 0.0;
    osc_term = 0.0;

    // Contribution from harmonic oscillator potential
    for (i = 0; i < 2; i++) {
        for (j = 0; j < 3; j++) {
            osc_term += r(i,j)*r(i,j);
        }
    }
    e_pot += 0.5*osc_term;

    // Contribution from the electron-electron repulsion
    for (j = 0; j < 3; j++) {
        r12 += (r(0,j)-r(1,j))*(r(0,j)-r(1,j));
    }
    r12 = sqrt(r12);
    e_pot += 1./r12;

    // Sum up local energy
    e_loc = e_kin + e_pot;

    return e_loc;
}
