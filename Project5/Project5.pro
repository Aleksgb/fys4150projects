TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    hermite.cpp \
    brutemc.cpp \
    legendre.cpp \
    impsamp.cpp \
    legpar.cpp \
    impsamppar.cpp \
    hermpar.cpp \
    brutemcpar.cpp \
    metropolis.cpp \
    wave_function.cpp \
    local_energy.cpp
SOURCES += lib.cpp

HEADERS +=  \
           hermite.h \
           lib.h \
    brutemc.h \
    legendre.h \
    impsamp.h \
    legpar.h \
    impsamppar.h \
    hermpar.h \
    brutemcpar.h \
    metropolis.h \
    wave_function.h \
    local_energy.h

QMAKE_CXX = mpicxx
QMAKE_CXX_RELEASE = $$QMAKE_CXX
QMAKE_CXX_DEBUG = $$QMAKE_CXX
QMAKE_LINK = $$QMAKE_CXX
QMAKE_CC = mpicc

QMAKE_CFLAGS += $$system(mpicc --showme:compile)
QMAKE_LFLAGS += $$system(mpicxx --showme:link)
QMAKE_CXXFLAGS += $$system(mpicxx --showme:compile) -DMPICH_IGNORE_CXX_SEEK
QMAKE_CXXFLAGS_RELEASE += $$system(mpicxx --showme:compile) -DMPICH_IGNORE_CXX_SEEK
