#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <cmath>
#include <armadillo>
#include <time.h>
#include "lib.h"

using namespace std;
using namespace arma;

double funcleg(double x1, double y1, double z1, double x2, double y2, double z2);

void legendre()
{
    clock_t start, finish;
    double a = -3.5;
    double b = 3.5;
    for (int n = 5; n <= 50; n += 5) {
        start = clock();
        double *x = new double [n];
        double *w = new double [n];
        gauleg(a, b, x, w, n);
        double integral = 0;

        for (int i=0; i<n; i++) {
            for (int j=0; j<n; j++) {
                for (int k=0; k<n; k++) {
                    for (int i2=0; i2<n; i2++) {
                        for (int j2=0; j2<n; j2++) {
                            for (int k2=0; k2<n; k2++) {
                                integral += w[i]*w[j]*w[k]*w[i2]*w[j2]*w[k2]*funcleg(x[i], x[j], x[k], x[i2], x[j2], x[k2]);
                            }
                        }
                    }
                }
            }
        }

        finish = clock();
        double cputime = (finish - start)/double(CLOCKS_PER_SEC);
        cout << "for n = " << n << " The integral is " << integral << " Time used is " << cputime << " seconds " << endl;
        delete [] x;
        delete [] w;
    }
}

double funcleg(double x1, double y1, double z1, double x2, double y2, double z2)
{
    double value;

    if (x1 == x2 && y1 == y2 && z1 == z2) {
        value = 0;
    }
    else {
        double x = x1-x2;
        double y = y1-y2;
        double z = z1-z2;
        value = exp(-(x1*x1 + y1*y1 + z1*z1 + x2*x2 + y2*y2 + z2*z2))/sqrt(x*x + y*y + z*z);
    }

    return value;
}
