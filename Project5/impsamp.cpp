#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <cmath>
#include <armadillo>
#include <time.h>
#include "lib.h"

using namespace std;
using namespace arma;

double funcimpsamp(double *x);

// function for gaussian random numbers
double gaussian_deviate(long *);

void impsamp()
{
    clock_t start, finish;
    long int i, j, n;
    long idum;
    double brute_mc, x[6], f, sum_sigma, variance, jacobidet;

    jacobidet = pow(acos(-1), 3);

    for (n = 1e0; n <= 1e9; n *= 10) {
        start = clock();
        brute_mc = 0;
        sum_sigma = 0;
        idum = -2;

        for (i = 1; i <= n; i++) {
            for (j = 0; j<=5; j++) {
                x[j] = gaussian_deviate(&idum)/sqrt(2.);
            }
            f = funcimpsamp(x);
            brute_mc += f;
            sum_sigma += f*f;
        }

        brute_mc /= (double) n;
        sum_sigma /= (double) n;
        variance = sum_sigma - brute_mc*brute_mc;
        finish = clock();
        double cputime = (finish - start)/double(CLOCKS_PER_SEC);
        cout << "For n = " << n << ", variance = " << jacobidet*sqrt(variance/(double) n) << ", integral = " << jacobidet*brute_mc << " Time used is " << cputime << " seconds " << endl;
    }
}


double funcimpsamp(double *x)
{
    double value;

    if (x[0] == x[1] && x[2] == x[3] && x[4] == x[5]) {
        value = 0;
    }
    else {
        double xx = x[0]-x[1];
        double y = x[2]-x[3];
        double z = x[4]-x[5];
        value = 1./sqrt(xx*xx + y*y + z*z);
    }

    return value;
}



// random numbers with gaussian distribution
double gaussian_deviate(long * idum)
{
  static int iset = 0;
  static double gset;
  double fac, rsq, v1, v2;

  if ( idum < 0) iset =0;
  if (iset == 0) {
    do {
      v1 = 2.*ran2(idum) -1.0;
      v2 = 2.*ran2(idum) -1.0;
      rsq = v1*v1+v2*v2;
    } while (rsq >= 1.0 || rsq == 0.);
    fac = sqrt(-2.*log(rsq)/rsq);
    gset = v1*fac;
    iset = 1;
    return v2*fac;
  } else {
    iset =0;
    return gset;
  }
} // end function for gaussian deviates




