from scitools.std import *
from numpy import loadtxt

t, sx, sy, svx, svy, ex, ey, evx, evy, jx, jy, jvx, jvy, E, L = loadtxt('threebody.dat', skiprows=1, unpack=True)
dt = t[1] - t[0]
T = len(t)*dt

plot(sx, sy, ex, ey, jx, jy, title=('Position of the earth, the sun and jupiter'), xlabel=('x [Au]'), ylabel=('y [Au]'), legend=('Sun', 'Earth', 'Jupiter'), hardcopy=('PosThreebodsunvel_dt=%g_T=%g.png' % (dt, T)))
'''
figure()

plot(sx, svx, ex, evx, jx, jvx, title=('Phase-diagram in x-direction for the earth, the sun and jupiter'), xlabel=('x [Au]'), ylabel=('vx [Au/yr]'), legend=('Sun', 'Earth', 'Jupiter'), hardcopy=('PhasexThreebod_dt=%g_T=%g.png' % (dt, T)))
figure()

plot(sy, svy, ey, evy, jy, jvy, title=('Phase-diagram in y-direction for the earth, the sun and jupiter'), xlabel=('y [Au]'), ylabel=('vy [Au/yr]'), legend=('Sun', 'Earth', 'Jupiter'), hardcopy=('PhaseyThreebod_dt=%g_T=%g.png' % (dt, T)))
figure()

plot(t, svx, t, svy, t, evx, t, evy, t, jvx, t, jvy, title=('Speed of the earth, the sun and jupiter as function of time'), xlabel=('t [yr]'), ylabel=('v [Au/yr]'), legend=('vx sun', 'vy sun', 'vx earth', 'vy earth', 'vx jupiter', 'vy jupiter'), hardcopy=('VelThreebod_dt=%g_T=%g.png' % (dt, T)))
figure()

E = (E - E[0])/abs(float(E[0]))
plot(t, E, title=('Relative error in the total energy compared with the initial energy'), xlabel=('t [yr]'), ylabel=('Relative Energy'), hardcopy=('RelEnThreebod_dt=%g_T=%g.png' % (dt, T)))
figure()

L = (L - L[0])/abs(float(L[0]))
plot(t, L, title=('Relative error in the total angular momentum compared with the initial angular momentum'), xlabel=('t [yr]'), ylabel=('Relative Angular Momentum'), hardcopy=('RelAngMomThreebod_dt=%g_T=%g.png' % (dt, T)))
'''