from scitools.std import *
from numpy import loadtxt

t, sx, sy, svx, svy, ex, ey, evx, evy, E, L = loadtxt('twobody.dat', skiprows=1, unpack=True)
dt = t[1] - t[0]
T = len(t)*dt

plot(sx, sy, ex, ey, title=('Position of the earth and the sun'), xlabel=('x [Au]'), ylabel=('y [Au]'), legend=('Sun', 'Earth'), hardcopy=('PosTwobod_dt=%g_T=%g.png' % (dt, T)))
figure()

plot(sx, svx, ex, evx, title=('Phase-diagram in x-direction for the earth and the sun'), xlabel=('x [Au]'), ylabel=('vx [Au/yr]'), legend=('Sun', 'Earth'), hardcopy=('PhasexTwobod_dt=%g_T=%g.png' % (dt, T)))
figure()

plot(sy, svy, ey, evy, title=('Phase-diagram in y-direction for the earth and the sun'), xlabel=('y [Au]'), ylabel=('vy [Au/yr]'), legend=('Sun', 'Earth'), hardcopy=('PhaseyTwobod_dt=%g_T=%g.png' % (dt, T)))
figure()

plot(t, svx, t, svy, t, evx, t, evy, title=('Speed of the earth and sun as function of time'), xlabel=('t [yr]'), ylabel=('v [Au/yr]'), legend=('vx sun', 'vy sun', 'vx earth', 'vy earth'), hardcopy=('VelTwobod_dt=%g_T=%g.png' % (dt, T)))
figure()

E = (E - E[0])/abs(float(E[0]))
plot(t, E, title=('Relative error in the total energy compared with the initial energy'), xlabel=('t [yr]'), ylabel=('Relative Energy'), hardcopy=('RelEnTwobod_dt=%g_T=%g.png' % (dt, T)))
figure()

L = (L - L[0])/abs(float(L[0]))
plot(t, L, title=('Relative error in the total angular momentum compared with the initial angular momentum'), xlabel=('t [yr]'), ylabel=('Relative Angular Momentum'), hardcopy=('RelAngMomTwobod_dt=%g_T=%g.png' % (dt, T)))
