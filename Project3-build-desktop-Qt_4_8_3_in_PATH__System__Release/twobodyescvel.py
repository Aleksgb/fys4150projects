from scitools.std import *
from numpy import loadtxt

t, sx, sy, svx, svy, ex, ey, evx, evy, E, L = loadtxt('twobodyescvel.dat', skiprows=1, unpack=True)
dt = t[1] - t[0]
T = len(t)*dt
vx0 = evx[0]
vy0 = evy[0]
v0 = sqrt(vx0**2 + vy0**2)

plot(sx, sy, ex, ey, title=('Position of the earth and the sun'), xlabel=('x [Au]'), ylabel=('y [Au]'), legend=('Sun', 'Earth'), hardcopy=('PosTwobodescvel_v0=%g_dt=%g_T=%g.png' % (v0, dt, T)))
