from scitools.std import *
m = array([1.66e-7, 2.448e-6, 3.0e-6, 3.2268e-7, 9.545e-4, 2.8578e-4, 4.3643e-5, 5.1496e-5, 6.5608e-9])
vmin = array([8.1699, 7.314, 6.158, 4.619, 2.615, 1.911, 1.364, 1.129, 0.78])
theta = array([350, 45, 20, 250, 285, 0, 75, 295, 295])
theta *= 2*pi/360
aphelion = array([0.4655, 0.7263, 1.014, 1.6615, 5.444, 10.097, 20.024, 30.304, 49.173])

comx = sum(m*cos(theta)*aphelion)/float(sum(m))
comy = sum(m*sin(theta)*aphelion)/float(sum(m))

v0x = m*sin(theta)*vmin
v0x = sum(v0x)
v0y = -m*cos(theta)*vmin
v0y = sum(v0y)
print v0x, v0y
    
theta0 = arctan(-v0x/float(v0y))
vmin0 = -v0x/sin(theta0)
vmin02 = v0y/cos(theta0)
aphelion0 = 6.23e-4

x = cos(theta)*aphelion + cos(theta0)*aphelion0
y = sin(theta)*aphelion + sin(theta0)*aphelion0
ap = sqrt(x**2 + y**2)
theta = arctan(y/x)
theta *= 360./(2*pi)

theta0 *= 360/(2.*pi)
print theta0, vmin0, vmin02
print ap
print theta