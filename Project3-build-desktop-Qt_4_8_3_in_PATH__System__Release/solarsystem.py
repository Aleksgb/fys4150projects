from scitools.std import *
from numpy import loadtxt

t, sux, suy, suvx, suvy, mex, mey, mevx, mevy, vx, vy, vvx, vvy, ex, ey, evx, evy, marx, mary, marvx, marvy, jx, jy, jvx, jvy, sax, say, savx, savy, ux, uy, uvx, uvy, nx, ny, nvx, nvy, px, py, pvx, pvy, E, L = loadtxt('solarsystem.dat', skiprows=1, unpack=True)
dt = t[1] - t[0]
T = len(t)*dt

plot(sux, suy, mex, mey, vx, vy, ex, ey, marx, mary, jx, jy, title=('Position of the different planets in the solar system'), xlabel=('x [Au]'), ylabel=('y [Au]'), legend=('Sun', 'Mercury', 'Venus', 'Earth', 'Mars', 'Jupiter'), hardcopy=('Possolsyssmall_dt=%g_T=%g.png' % (dt, T)))
figure()
'''
plot(sux, suvx, mex, mevx, vx, vvx, ex, evx, marx, marvx, jx, jvx, sax, savx, ux, uvx, nx, nvx, px, pvx, title=('Phase-diagram in x-direction for the different planets'), xlabel=('x [Au]'), ylabel=('vx [Au/yr]'), legend=('Sun', 'Mercury', 'Venus', 'Earth', 'Mars', 'Jupiter', 'Saturn', 'Uranus', 'Neptune', 'Pluto'), hardcopy=('Phasexsolsys_dt=%g_T=%g.png' % (dt, T)))
figure()

plot(suy, suvy, mey, mevy, vy, vvy, ey, evy, mary, marvy, jy, jvy, say, savy, uy, uvy, ny, nvy, py, pvy, title=('Phase-diagram in y-direction for the different planets'), xlabel=('y [Au]'), ylabel=('vy [Au/yr]'), legend=('Sun', 'Mercury', 'Venus', 'Earth', 'Mars', 'Jupiter', 'Saturn', 'Uranus', 'Neptune', 'Pluto'), hardcopy=('Phaseysolsys_dt=%g_T=%g.png' % (dt, T)))
figure()

E = (E - E[0])/E[0]
plot(t, E, title=('Relative error in the total energy compared with the initial energy'), xlabel=('t [yr]'), ylabel=('Relative Energy'), hardcopy=('RelEnThreebod_dt=%g_T=%g.png' % (dt, T)))
figure()

L = (L - L[0])/L[0]
plot(t, L, title=('Relative error in the total angular momentum compared with the initial angular momentum'), xlabel=('t [yr]'), ylabel=('Relative Angular Momentum'), hardcopy=('RelAngMomThreebod_dt=%g_T=%g.png' % (dt, T)))

