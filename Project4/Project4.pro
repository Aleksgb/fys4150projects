TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    tridiag.cpp \
    testing.cpp \
    diffusion.cpp

HEADERS += \
    tridiag.h \
    testing.h \
    diffusion.h

