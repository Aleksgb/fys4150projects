#ifndef TESTING_H
#define TESTING_H

#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <cmath>
#include <armadillo>
#include <time.h>
#include "tridiag.h"

using namespace std;
using namespace arma;

void testing();

#endif // TESTING_H
