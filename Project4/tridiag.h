#ifndef TRIDIAG_H
#define TRIDIAG_H

#include <armadillo>
#include <math.h>

using namespace arma;
using namespace std;

vec tridiag(int, double, double, double, vec&);

#endif // TRIDIAG_H
