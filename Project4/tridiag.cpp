#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <cmath>
#include <armadillo>
#include <time.h>

using namespace arma;
using namespace std;

vec tridiag(int n, double A, double B, double C, vec &vold)
{    /* Function which contains our tridiagonal solver.
        Input is number of points, the elements in a, b and c,
        the right-hand side of the equation A*v_i+1 = v_i.
        The function fills v_i+1 with the right values and
        returns this vector*/

    int i;
    double alpha;
    vec vnew(n+2), a(n+2), b(n+2), c(n+2);
    vnew.fill(0);
    a.fill(A);
    b.fill(B);
    c.fill(C);


    for (i=2; i<=n; i++) {
        alpha = a(i)/b(i-1);
        b(i) = b(i) - alpha*c(i-1);
        vold(i) = vold(i) - alpha*vold(i-1); // Forward substitution
    }

    vnew(n) = vold(n)/b(n);

    for (i=n; i>=2; i--) {
        vnew(i-1) = (vold(i-1)-c(i-1)*vnew(i))/b(i-1);  // Backward substitution
    }

    return vnew;
}

