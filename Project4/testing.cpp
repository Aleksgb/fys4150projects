#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <cmath>
#include <armadillo>
#include <time.h>
#include "tridiag.h"

using namespace std;
using namespace arma;

void testing()
{
    ofstream outFile;
    outFile.open("diffusion.dat", ios::out);
    int n = 10;
    int tsteps = 1000*n;
    double dx = 1./n;
    double dt = 0.5*dx*dx;
    double alpha = dt/(dx*dx);
    int timestep0 = 10;
    int timestep1 = 100;
    vec vfe(n+1), vbe(n+1), vcn(n+1), vfetemp(n+1), vcntemp(n+1);
    vec v0fe(n+1), v0be(n+1), v0cn(n+1);
    vec v1fe(n+1), v1be(n+1), v1cn(n+1);
    vfetemp.fill(0);
    vcntemp.fill(0);
    double a = 1 + 2*alpha;
    double a2 = 2 + 2*alpha;
    double b = -alpha;

    // Create the initial vs
    for (int i = 1; i < n; i++) {
        vfe(i) = i*dx - 1;
    }
    vfe(0) = vfe(n) = 0;
    vbe = vfe;
    vcn = vfe;

    // Loop over the timesteps
    for (int i=0; i<tsteps; i++) {
        // Store the arrays correpsonding to t = t1 and t = t2
        if (i == timestep0) {
            v0fe = vfe;
            v0be = vbe;
            v0cn = vcn;
            }
        if (i == timestep1) {
            v1fe = vfe;
            v1be = vbe;
            v1cn = vcn;
        }


        // Compute the next arrays for the forward Euler method
        for (int k=1; k<n; k++) {
            vfetemp(k) = alpha*vfe(k-1) + (1-2*alpha)*vfe(k) + alpha*vfe(k+1);
        }
        vfe = vfetemp;


        // Compute the next arrays for the backward Euler method
        vbe = tridiag(n-1, b, a, b, vbe);
        vbe(0) = vbe(n) = 0;

        // Compute the next arrays for the Crank Nicholson method

        for (int k=1; k<n; k++) {
            vcntemp(k) = alpha*vcn(k-1) + (2-2*alpha)*vcn(k) + alpha*vcn(k+1);
        }

        vcn = tridiag(n-1, b, a2, b, vcntemp);
        vcn(0) = vcn(n) = 0;

    }

    // Write the arrays corresponding to t = t1 and t = t2 to file
    for (int i=0; i<=n; i++) {
        outFile << i*dx << " " << v0fe(i) << " " << v1fe(i) << " " << v0be(i) << " " << v1be(i) << " " << v0cn(i) << " " << v1cn(i) << endl;
    }

    system("python diffusion.py");
}
