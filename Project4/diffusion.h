#ifndef DIFFUSION_H
#define DIFFUSION_H

#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <cmath>
#include <armadillo>
#include <time.h>
#include "tridiag.h"

using namespace std;
using namespace arma;

void diffusion();

#endif // DIFFUSION_H
