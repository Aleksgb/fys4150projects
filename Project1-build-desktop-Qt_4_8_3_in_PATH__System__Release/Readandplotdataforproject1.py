from scitools.std import *

infile = open('Dataforproject1.dat', 'r')
infile.readline()
words = [line.split() for line in infile]
v1, u1, v2, u2, v3, u3, v4, u4, v5, u5 = words[0], words[1], words[2], words[3], words[4], words[5], words[6], words[7], words[8], words[9]

v1 = array([float(element) for element in v1])
u1 = array([float(element) for element in u1])
v2 = array([float(element) for element in v2])
u2 = array([float(element) for element in u2])
v3 = array([float(element) for element in v3])
u3 = array([float(element) for element in u3])
v4 = array([float(element) for element in v4])
u4 = array([float(element) for element in u4])
v5 = array([float(element) for element in v5])
u5 = array([float(element) for element in u5])

x1 = array([i/(10+1.) for i in range(10+2)])
x2 = array([i/(100+1.) for i in range(100+2)])
x3 = array([i/(1000+1.) for i in range(1000+2)])
x4 = array([i/(10000+1.) for i in range(10000+2)])
x5 = array([i/(100000+1.) for i in range(100000+2)])

plot(x1, v1, '--or', x1, u1, '-b', xlabel='x', ylabel='u', title='Plot of analytical and numerical solution for n=10', legend=('Numerical', 'Analytical'), hardcopy='compare_n=10.png')
figure()
plot(x2, v2, 'or', x2, u2, '-b', xlabel='x', ylabel='u', title='Plot of analytical and numerical solution for n=100', legend=('Numerical', 'Analytical'), hardcopy='compare_n=100.png')
figure()
plot(x3, v3, 'or', x3, u3, '-b', xlabel='x', ylabel='u', title='Plot of analytical and numerical solution for n=1000', legend=('Numerical', 'Analytical'), hardcopy='compare_n=1000.png')
figure()
plot(x4, v4, 'or', x4, u4, '-b', xlabel='x', ylabel='u', title='Plot of analytical and numerical solution for n=10000', legend=('Numerical', 'Analytical'), hardcopy='compare_n=10000.png')
figure()
plot(x5, v5, 'or', x5, u5, '-b', xlabel='x', ylabel='u', title='Plot of analytical and numerical solution for n=100000', legend=('Numerical', 'Analytical'), hardcopy='compare_n=100000.png')
figure()

infile.close()


infile = open('Dataforproject12.dat', 'r')
infile.readline()

epsvalues = []
hvalues = []

for line in infile:
    words = line.split()
    epsvalues.append(float(words[0]))
    hvalues.append(float(words[1]))

epsvalues = array(epsvalues)
hvalues = array(hvalues)

plot(hvalues, epsvalues, '-ro', xlabel='Log10(h)', ylabel='maks(epsilon_i)', title='Plot of the relative error as function of log10(h)', hardcopy='relative_error.png')

infile.close()

infile = open('Dataforproject13.dat', 'r')
infile.readline()

hvalues = []
ludec = []
algo = []

for line in infile:
    words = line.split()
    hvalues.append(float(words[0]))
    ludec.append(float(words[1]))
    algo.append(float(words[2]))

hvalues = array(hvalues)
ludec = array(ludec)
algo = array(algo)

plot(hvalues, ludec, '-ro', hvalues, algo, '-bo', xlabel='log10(steplength)', ylabel='Time used [s]', title='Plot of the time usage by the two methods as function of log10(h)', legend=('Armadillos functions', 'My algorithm'), hardcopy='time_usage.png')

infile.close()
