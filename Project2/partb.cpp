#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <cmath>
#include <armadillo>
#include <time.h>
#include "jacobismethod.h"

using namespace arma;
using namespace std;

void partb()
{
    ofstream outFile;
    outFile.open("Project2niterations.dat", ios::out);
    outFile << "First column is dimensionality of matrix A: n_step-1. Second column is number of iterations, i. Third column is the time used with Armadillo and the fourth column is the time needed with Jacobis algorithm" << endl;
    int it;
    clock_t start, finish;
    double Armatime, Jacobitime;

    /* Now we create the matrix A for different values of nstep and
       solves the eigenvalue problem both with armadillo and jacobis method
       compute the time usage, give the eigenvalues and writes the number of stepst
       the number of iterations, and the time usage to file */

    for (int nstep = 11; nstep < 300; nstep += 5) {
        double rhomin = 0.0, rhomax = 5.0;
        double h = (rhomax - rhomin)/(double) nstep;
        vec d(nstep-1); vec rho(nstep-1); vec e(nstep-2); vec eigval; mat eigvec;
        e.fill(-1.0/(h*h));

        for (int i = 0; i < nstep-1; i++) {
            rho(i) = rhomin + h + i*h;
            d(i) = 2./(h*h) + rho(i)*rho(i);
        }

        mat R(nstep - 1, nstep - 1);
        mat A(nstep - 1, nstep - 1);
        A.fill(0);
        A.diag(0) = d;
        A.diag(-1) = e;
        A.diag(1) = e;


        start = clock();
        eig_sym(eigval, eigvec, A);
        finish = clock();
        Armatime = (finish-start)/(double) CLOCKS_PER_SEC;
        cout << "Time needed for Armadillo-algo: " << Armatime << endl;
        cout << "eigenvalues: \n" << eigval.subvec(0, 3) << endl;


        start = clock();
        jacobis_method(A, R, nstep - 1, &it);
        finish = clock();
        Jacobitime = (finish-start)/(double) CLOCKS_PER_SEC;
        cout << "Time needed for Jacobis method: " << Jacobitime << endl;
        vec lambda = A.diag(0);

        for (int i = 0; i < 4; i++) {
            double minvalue = min(lambda);
            lambda = lambda.elem(find(lambda > minvalue));
            cout << setprecision(4) << "Eigenvalue number " << i+1 << ": " << minvalue << endl;
        }
        cout << endl;

        outFile << nstep - 1 << " " << it << " " << Armatime << " " << Jacobitime << endl;
    }
}

