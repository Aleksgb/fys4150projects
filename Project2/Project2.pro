TEMPLATE = app
CONFIG += console
CONFIG -= qt

SOURCES += \
    jacobismethod.cpp \
    main.cpp \
    partb.cpp \
    testing.cpp \
    partc.cpp \
    partd.cpp \
    lib.cpp

LIBS += -larmadillo -lblas -llapack

HEADERS += \
    jacobismethod.h \
    partb.h \
    testing.h \
    partc.h \
    partd.h \
    lib.h
