#ifndef JACOBISMETHOD_H
#define JACOBISMETHOD_H

#include <armadillo>
using namespace arma;

void jacobis_method(mat &, mat &, int, int *);
double max_off(mat &, int *, int *, int);
void rotate(mat &, mat &, int, int, int);

#endif // JACOBISMETHOD_H


