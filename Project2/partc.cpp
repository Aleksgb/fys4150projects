#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <cmath>
#include <armadillo>
#include <time.h>
#include "jacobismethod.h"

using namespace arma;
using namespace std;

void partc()
{

    /* Here we do the same as in b, but with a different potential and
       only nstep and the eigenvalue written to file */

    ofstream outFile;
    outFile.open("wr=0.25.dat", ios::out);
    outFile << "First column is nstep, second column is the first eigenvalue." << endl;
    for (int nstep = 11; nstep <= 300; nstep += 40) {
        int it;
        double rhomin = 0.0, rhomax = 2.0, Jacobitime = 0.0, wr = 0.25;
        double h = (rhomax - rhomin)/(double) nstep;
        clock_t start, finish;
        vec d(nstep-1); vec rho(nstep-1); vec e(nstep-2);
        e.fill(-1.0/(h*h));

        for (int i = 0; i < nstep-1; i++) {
            rho(i) = rhomin + h + i*h;
            d(i) = 2./(h*h) + wr*wr*rho(i)*rho(i) + 1.0/rho(i);
        }

        mat R(nstep - 1, nstep - 1);
        mat A(nstep - 1, nstep - 1);
        A.diag(0) = d;
        A.diag(-1) = e;
        A.diag(1) = e;

        start = clock();
        jacobis_method(A, R, nstep - 1, &it);
        finish = clock();
        Jacobitime = (finish-start)/(double) CLOCKS_PER_SEC;
        cout << "Time needed for Jacobis method: " << Jacobitime << endl;
        vec lambda = A.diag(0);
        double minvalue = min(lambda);
        cout << setprecision(6) << "Eigenvalue number 1: " << minvalue << endl << endl;
        outFile << nstep << " " << minvalue << endl;

    }
}

