#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <cmath>
#include <armadillo>
#include <time.h>
#include "jacobismethod.h"

using namespace arma;
using namespace std;

void partd()
{

    /* Here we also find the normalized eigenvectors, and write
       rho and the eigenvectors to file */

    ofstream outFile;
    outFile.open("Eigenvecnonintwr=5.0.dat", ios::out);
    outFile << "First column is rho, second is the eigenvector \n";
    int it, nstep = 100;
    double rhomin = 0.0, rhomax = 2.0, wr = 5.0;
    double h = (rhomax - rhomin)/(double) nstep;
    vec d(nstep-1); vec rho(nstep-1); vec e(nstep-2);
    e.fill(-1.0/(h*h));

    for (int i = 0; i < nstep-1; i++) {
        rho(i) = rhomin + h + i*h;
        d(i) = 2./(h*h) + wr*wr*rho(i)*rho(i);
    }

    mat R(nstep - 1, nstep - 1);
    mat A(nstep - 1, nstep - 1);
    uword index;
    vec eigvec(nstep-1);
    double eigval;
    A.diag(0) = d;
    A.diag(-1) = e;
    A.diag(1) = e;

    jacobis_method(A, R, nstep - 1, &it);

    eigvec = A.diag(0);
    eigval = eigvec.min(index);
    eigvec = R.col(index);

    cout << "Eigenvalue:\n" << eigval << endl;

    double norm = 0.0;
    for (int i = 0; i < nstep-1; i++) {
        norm += eigvec(i)*eigvec(i);
    }
    eigvec /= sqrt(h*norm);


    for (int i = 0; i < nstep-1; i++) {
        outFile << rho(i) << " " << eigvec(i) << endl;
    }
}
