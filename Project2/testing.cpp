#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <cmath>
#include <armadillo>
#include <time.h>
#include "jacobismethod.h"
#include "lib.h"

using namespace arma;
using namespace std;

void testing() {
    mat r(2, 3);
    double e_pot = 0;
    double r12 = 0;
    r(0, 0) = 1;
    r(1, 0) = 2;
    r(0, 1) = 3;
    r(1, 1) = 4;
    r(0, 2) = 5;
    r(1, 2) = 6;
    cout << r;
    for (int j = 0; j < 3; j++) {
        r12 += (r(0,j)-r(1,j))*(r(0,j)-r(1,j));
    }
    cout << r12 << endl;
    cout << 1./sqrt(r12) << endl;
    r12 = sqrt(r12);
    cout << r12 << endl;

    e_pot += 1./r12;
    cout << e_pot;

}
