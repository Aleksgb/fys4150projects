#include <iostream>
#include <math.h>
#include <cmath>
#include <armadillo>

using namespace arma;
using namespace std;

double maxoff(mat &, int *, int *, int);
void rotate(mat &, mat &, int, int, int);


// Function which takes in the matrix A, an empty matrix R for eigenvectors and the number of steps n
void jacobis_method(mat &A, mat &R, int n, int *it) {
    R = eye(n,n);
    int k, l;
    double eps = 1.0e-8;
    double max_it = pow((double) n, 3);
    int iterations = 0;
    double max_off = maxoff(A, &k, &l, n);

    while (max_off > eps && (double) iterations < max_it) {
        max_off = maxoff(A, &k, &l, n);
        rotate(A, R, k, l, n);
        iterations++;
    }
    *it = iterations;

    cout << "The number of iterations for n = " << n << " is i = " << iterations << endl;
}


// Function which finds the maximum off-diagonal element of A and stores the indices of this element
double maxoff(mat &A, int *k, int *l, int n) {
    mat B = abs(A);
    B.diag(0) = zeros(n);
    uword row;
    uword col;
    double max_off = B.max(row, col);
    *l = row;
    *k = col;

    return max_off;
}


// Function which finds tau, t, c, s, performs the rotation-algorithm and computes the new eigenvectors
void rotate(mat &A, mat&R, int k, int l, int n) {
    double s, c;

    if (A(k,l) != 0.0) {
        double t, tau;
        tau = (A(l,l)-A(k,k))/(2*A(k,l));
        if (tau > 0) {
            t = 1.0/(tau + sqrt(1 + tau*tau));
        }
        else {
            t = -1.0/(-tau + sqrt(1 + tau*tau));
        }
        c = 1.0/sqrt(1 + t*t);
        s = c*t;
    }
    else {
        c = 1.0;
        s = 0.0;
    }

    double a_kk, a_ll, a_ik, a_il, r_ik, r_il;
    a_kk = A(k,k);
    a_ll = A(l,l);
    // Changing the elements with indices k and l
    A(k,k) = c*c*a_kk - 2.0*c*s*A(k,l) + s*s*a_ll;
    A(l,l) = s*s*a_kk + 2.0*c*s*A(k,l) + c*c*a_ll;
    A(k,l) = 0.0;
    A(l,k) = 0.0;
    // Changing the remaining elements
    for (int i = 0; i < n; i++) {
        if (i != k && i != l) {
            a_ik = A(i,k);
            a_il = A(i,l);
            A(i,k) = c*a_ik - s*a_il;
            A(k,i) = A(i,k);
            A(i,l) = c*a_il + s*a_ik;
            A(l,i) = A(i,l);
        }
        // Compute the new eigenvectors
        r_ik = R(i,k);
        r_il = R(i,l);
        R(i,k) = c*r_ik - s*r_il;
        R(i,l) = c*r_il + s*r_ik;
    }

}

