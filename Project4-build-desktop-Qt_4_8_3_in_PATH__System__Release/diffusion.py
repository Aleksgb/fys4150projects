from scitools.std import *
from numpy import loadtxt

x, v0fe, v1fe, v0be, v1be, v0cn, v1cn = loadtxt('diffusion.dat', unpack=True)
n = 10
dx = 1./n
dt = 0.5*dx**2
t0 = 10*dt
t1 = 100*dt

u0fe = v0fe + 1 - x
u0be = v0be + 1 - x
u0cn = v0cn + 1 - x
u1fe = v1fe + 1 - x
u1be = v1be + 1 - x
u1cn = v1cn + 1 - x

#exact0 = zeros(n+1)
exact1 = zeros(n+1)
#for n in range(1, 10000):
#    exact0 += -2.0/(n*pi)*sin(n*pi*x)*exp(-n**2*pi**2*t0)
for n in range(1, 10000):
    exact1 += -2.0/(n*pi)*sin(n*pi*x)*exp(-n**2*pi**2*t1)
#exactu0 = exact0 + 1 - x
exactu1 = exact1 + 1 - x

#relerru0fe = abs(u0fe - exactu0)
relerru1fe = abs(u1fe - exactu1)
#relerru0be = abs(u0be - exactu0)
relerru1be = abs(u1be - exactu1)
#relerru0cn = abs(u0cn - exactu0)
relerru1cn = abs(u1cn - exactu1)

#plot(x, u0fe, x, u0be, x, u0cn, x, exactu0, title=('u(x, t0 = %g), dx = %g, dt = %g' % (t0, dx, dt)), xlabel=('Distance'), ylabel=('Concentration'), legend=('Forward Euler', 'Backward Euler', 'Crank Nicholson', 'Exact'), hardcopy=('diffusion_t0=%g_dx=%g_dt=%g.png' % (t0, dx, dt)))
#figure()
plot(x, u1fe, x, u1be, x, u1cn, x, exactu1, title=('u(x, t1 = %g), dx = %g, dt = %g' % (t1, dx, dt)), xlabel=('Distance'), ylabel=('Concentration'), legend=('Forward Euler', 'Backward Euler', 'Crank Nicholson', 'Exact'), hardcopy=('diffusion_t1=%g_dx=%g_dt=%g.png' % (t1, dx, dt)))
figure()
#plot(x, relerru0fe, x, relerru0be, x, relerru0cn, title=('Absolute error for u(x, t0 = %g), dx = %g, dt = %g' %(t0, dx, dt)), xlabel=('Distance'), ylabel=('Absolute error'), legend=('Forward Euler', 'Backward Euler', 'Crank Nicholson'), hardcopy=('abserr_t0=%g_dx=%g_dt=%g.png' % (t0, dx, dt)))
#figure()
plot(x, relerru1fe, x, relerru1be, x, relerru1cn, title=('Absolute error for u(x, t1 = %g), dx = %g, dt = %g' %(t1, dx, dt)), xlabel=('Distance'), ylabel=('Absolute error'), legend=('Forward Euler', 'Backward Euler', 'Crank Nicholson'), hardcopy=('abserr_t1=%g_dx=%g_dt=%g.png' % (t1, dx, dt)))
