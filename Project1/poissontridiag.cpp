#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <cmath>
#include <armadillo>
#include <time.h>

using namespace arma;
using namespace std;

void poisson(int, vec &);

int main()
{
    ofstream outFile;
    outFile.open("Dataforproject1.dat", ios::out);
    clock_t start, finish;
    int n, i, j, k;
    vec v, u;
    mat L, U, A;

    //First we write v and u to file for n=10, 100, 1000, 10000 and 100000

    outFile << "# Data for project 1: First line is v(i) (i=0,1,...,n+1), second is u(i) and this is repeated for n=10, 100, ..., 100000\n";

    for (n=10; n<=100000; n*=10) {
        v = zeros(n+2);
        u = zeros(n+2);
        poisson(n, v);   // Call the function Poisson to get the array v filled with elements
        for (i=0; i<=n+1; i++) {
            outFile << v(i) << " "; // Write v to file
        }

        outFile << endl;

        for (i=0; i<=n+1; i++) {
            u(i) = 1 - (1-exp(-10.0))*i/(n+1) - exp(-10.0*i/(n+1)); // Analytical solution
            outFile << u(i) << " "; // Write u to file
        }

        outFile << endl;

    }
    outFile.clear(); // Close the file
    outFile.close();

    // Then we write log10(max(relative error)) and log10(h) to file

    outFile.open("Dataforproject12.dat", ios::out);

    outFile << "# Data for project1: First column is max(relative error). Second column is log10(h)";

    for (n=10; n<=100000; n+=500) {
        v = zeros(n+2);
        u = zeros(n+2);
        vec epsilon(n+2);
        poisson(n, v);

        for (i=0; i<=n+1; i++) {
            u(i) = 1 - (1-exp(-10.0))*i/(n+1) - exp(-10.0*i/(n+1));
        }

        for (i=1; i<=n+1; i++) {
            epsilon(i) = log10(fabs((v(i) - u(i))/u(i))); // Calculate log10(relative error)
        }
        double maxeps = 0.0;
        for (i=1; i<=n+1; i++) {
            if (fabs(maxeps)<=fabs(epsilon(i))) maxeps = epsilon(i); // Find maxvalue of error
        }

        outFile << setprecision(15) << maxeps << " " << log10(1./(n+1)) << endl;

    }

    outFile.clear();
    outFile.close();

    // Then we write log10(h) (different hs), time used by armadillos functions and time used by our algorithm to file

    outFile.open("Dataforproject13.dat", ios::out);

    outFile << "# Data for project1: First column is log10(h). Second column is time used by armadillos lu decomposition and solver, third column is time used by my algorithm";

    for (n=10; n<= 9000; n+=500) {
        vec a(n+1), f(n+2);
        a.fill(-1);
        A = 2*eye(n+2,n+2); A.diag(1) = a; A.diag(-1) = a; // Initialize the matrix A with armadillos functionality

        for (i=1; i<=n; i++) {
            f(i) = 100*exp(-10.0*i/(n+1))/((n+1)*(n+1));
        }
        A(0,0)=1; A(1,0)=0; A(0,1)=0; A(n+1,n+1)=1; A(n,n+1)=0; A(n+1,n)=0; f(0)=0; f(n+1) = 0; // We have to do this to get the right array v

        start = clock();

        lu(L, U, A);              // Find v with Armadillo
        vec Ux = solve(L, f);
        vec x = solve(U, Ux);

        finish = clock();
        outFile << setprecision(15) << log10(1./(n+1)) << " " << (finish - start)/double(CLOCKS_PER_SEC) << " "; // Write time used to file
        v = zeros(n+2);
        start = clock();
        poisson(n, v);        // Find v with our algorithm
        finish = clock();
        outFile << setprecision(15) << (finish - start)/double(CLOCKS_PER_SEC) << endl;  // Write time used to file

        outFile.clear();
        outFile.close();
}

    // Then we do a matrix multiplication in three different ways and print out the time used

    n = 3000;
    mat B = zeros(n,n);
    mat B2 = zeros(n,n);
    mat B3 = zeros(n,n);
    mat C = randu<mat>(n,n);
    mat D = randu<mat>(n,n);
    start = clock();

    for (i=0; i<n; i++) {
        for (j=0; j<n; j++) {
            for (k=0; k<n; k++) {
                B(i,j) += C(i,k)*D(k,j);   // Row-major ordering
            }
        }
    }
    finish = clock();
    cout << "Time used with row-major ordering for n = " << n << ": t = " << (finish-start)/double(CLOCKS_PER_SEC) << " seconds." << endl;

    start = clock();
    for (k=0; k<n; k++) {
        for (i=0; i<n; i++) {
            for (j=0; j<n; j++) {
                B2(i,j) += C(i,k)*D(k,j);  // Column-major ordering
            }
        }
    }
    finish = clock();
    cout << "Time used with column-major ordering for n = " << n << ": t = " << (finish-start)/double(CLOCKS_PER_SEC) << " seconds." << endl;

    start = clock();
    B3 = C*D;               // Standard Armadillo product
    finish = clock();
    cout << "Time used with standard Armadillo multiplication for n = " << n << ": t = " << (finish-start)/double(CLOCKS_PER_SEC) << " seconds." << endl;

    return 0;

}

void poisson(int n, vec &v)   // Function which contains our algorithm. Takes in number of points, array v and fills v with the right values
{

    int i;
    double alpha;
    vec f(n+2), a(n+2), b(n+2), c(n+2);
    f.fill(0);
    a.fill(-1);
    b.fill(2);
    c.fill(-1);

    for (i=0; i<=n+1; i++) {
        f(i) = 100*exp(-10.0*i/(n+1))/((n+1)*(n+1)); // Analytical solution

    }


    for (i=2; i<=n; i++) {
        alpha = a(i)/b(i-1);
        b(i) = b(i) - alpha*c(i-1);
        f(i) = f(i) - alpha*f(i-1); // Forward substitution
    }

    v(n) = f(n)/b(n);

    for (i=n; i>=2; i--) {
        v(i-1) = (f(i-1)-c(i-1)*v(i))/b(i-1);  // Backward substitution
    }
}
